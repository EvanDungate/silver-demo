from __future__ import print_function
from __future__ import division

#####################################################################
from builtins import range
from past.utils import old_div
# from SILVER_VER_18.scenario_analysis_module import scenario_metrics
from SILVER_VER_18.config_variables import *
from SILVER_VER_18.setup_call_for_week import setup_call_for_week
from SILVER_VER_18.visualization import *
#####################################################################
from time import perf_counter
import datetime
############################


list_of_failed_uc_days = []
##############
# MAIN - calls all other functions
##############
starttime = perf_counter()


def main():

    print("Beginning analysis from:", USER_STARTDATE.date(), "to", USER_ENDDATE.date(), USER_ENDDATE - USER_STARTDATE, 'hours')
    num_commitment_periods = old_div((USER_ENDDATE - USER_STARTDATE).days, NUMBER_OF_DAYS)
    startdate = USER_STARTDATE

    enddate = startdate + datetime.timedelta(days=NUMBER_OF_DAYS)
    for start_of_period_run in range(num_commitment_periods):

        period_starttime = perf_counter()
        list_of_failed_uc_days_2 = setup_call_for_week(startdate, enddate, start_of_period_run, list_of_failed_uc_days)
        startdate = startdate + datetime.timedelta(days=NUMBER_OF_DAYS)
        enddate = enddate + datetime.timedelta(days=NUMBER_OF_DAYS)

        print("month time: %f seconds " % (perf_counter() - period_starttime))


print("total time: %f seconds " % (perf_counter() - starttime))


if __name__ == '__main__':

    main()
